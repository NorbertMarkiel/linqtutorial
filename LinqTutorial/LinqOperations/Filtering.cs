﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqTutorial.LinqOperations
{
     class Filtering
    {
        public static void WhereQuerySyntax(IList<Student> studentList)
        {
            Console.WriteLine("Where Query Syntax \n\nTeen Age Students:");
            var teenAgerStudent = from s in studentList
                                  where s.Age > 12 && s.Age < 20
                                  select s;
            foreach (var item in teenAgerStudent)
            {
                Console.WriteLine(item.StudentName);
            }

            // Var to IList
            //   IList<Student> result = teenAgerStudent.ToList();
        }
        public static void WhereMethodSyntax(IList<Student> studentList)
        {
            Console.WriteLine("Where Method Syntax \n\nTeen Age Students:");
            var teenAgerStudent = studentList.Where(s => s.Age > 12 && s.Age < 20);
            foreach (var item in teenAgerStudent)
            {
                Console.WriteLine("Name:{0} Age:{1}",item.StudentName,item.Age);
            }

        }



    }
}
