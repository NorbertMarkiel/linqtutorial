﻿using LinqTutorial.LinqOperations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqTutorial
{
    class Program
    {

        static void Main(string[] args)
        {
            IList<Student> studentList = Init.CreateStudents();
            Filtering.WhereQuerySyntax(studentList);
            Filtering.WhereMethodSyntax(studentList);
        }
    }
}
